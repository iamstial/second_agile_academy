﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntroToEFCoreFinal.Domain;
using IntroToEFCoreFinal.EFCDomain;
using IntroToEFCoreFinal.EFCDomain.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Person>> GetWell()
        {
            IQueries blao = new EFCServices();
            var q = blao.Query1();
            return q.ToList();
        }

    }

    [Route("api/[controller]")]
    [ApiController]
    public class GiwrgosController : ControllerBase
    {
        // GET api/giwrgos
        [HttpGet]
        public ActionResult<IEnumerable<SalesOrderHeader>> GetBetter()
        {
            IQueries blao = new EFCServices();
            var q2 = blao.Query2();
            return q2.ToList();
        }
    }
}