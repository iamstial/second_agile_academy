﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Models
{
    public class ModelClass
    {
        public string MVCFirstName { get; set; }

        public string MVCLastName { get; set; }
    }
}
