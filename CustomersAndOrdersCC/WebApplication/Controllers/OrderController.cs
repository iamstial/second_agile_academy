﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomersAndOrdersCC.Data.Models;
using CustomersAndOrdersCC.NewData.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    public class OrderController : Controller
    {
        [HttpPost]
        public IActionResult FinalOrder(CombinedModel Combo)
        {
            
            //get filled Combo data from Session
            var FetchCombo = HttpContext.Session.GetObjectFromJson<CombinedModel>("_Combo");

            //add order userinput to combo data
            for (int i = 0; i < FetchCombo.Oproduct.Count(); ++i)
            {
                FetchCombo.Oproduct[i].Quantity = Combo.Oproduct[i].Quantity; 
            }

            FetchCombo.Oproduct.RemoveAll(x => x.Quantity == 0);

            //generate new GUid for our order
            FetchCombo.OrderID = Guid.NewGuid();

            using (var db = new MyNewDatabaseContext())
            {
                var orderDB = db.Set<Order>();
                var orderProductzDB = db.Set<OrderProducts>();


                orderDB.Add(new Order
                {
                    OrderId = FetchCombo.OrderID,
                    PersonId = FetchCombo.PersInfo.PersonID,
                    DateTime = DateTime.Now
                });


                for (int i = 0; i < FetchCombo.Oproduct.Count(); ++i)
                {
                    orderProductzDB.Add(new OrderProducts
                    {
                      ProductId = FetchCombo.Oproduct[i].ProductId,
                      OrderId = FetchCombo.OrderID,
                      Quantity = FetchCombo.Oproduct[i].Quantity
                    });
                }
                



                db.SaveChanges();
            }


            return View("CompletedOrder", FetchCombo);
        }
    }
}