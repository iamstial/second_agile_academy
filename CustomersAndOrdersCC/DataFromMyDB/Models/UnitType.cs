﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class UnitType
    {
        public UnitType()
        {
            Product = new HashSet<Product>();
        }

        public int UnitTypeId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Product> Product { get; set; }
    }
}
