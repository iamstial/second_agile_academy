﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class AddressType
    {
        public AddressType()
        {
            CustomerAddressCon = new HashSet<CustomerAddressCon>();
        }

        public int AddressTypeId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<CustomerAddressCon> CustomerAddressCon { get; set; }
    }
}
