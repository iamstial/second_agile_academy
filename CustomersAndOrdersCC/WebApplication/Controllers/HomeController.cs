﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using CustomersAndOrdersCC.Data;
using CustomersAndOrdersCC.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using WebApplication.Models;


namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {        
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Comparison(ModelClass model)
        {

            HttpContext.Session.SetString("FirstName", model.MVCFirstName);
            HttpContext.Session.SetString("LastName", model.MVCLastName);

            var efcservice = new NewEFCService();
            List<Customer> PersInfoOld = new List<Customer>();
            List<Customer> PersInfo = new List<Customer>();
            NewCustomer ItemNewInfo = new NewCustomer();
            List<NewCustomer> NewInfo = new List<NewCustomer>();

            PersInfo = efcservice.SearchPerson(model.MVCFirstName, model.MVCLastName);
            
            //next time persinfo is filled (from old db) append instead of replace( the new)
            
            //change personID to uppercase



            string Baseurl = "http://localhost:5000/";

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                //no need
                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient 
                //var
                HttpResponseMessage Res = await client.GetAsync("api/values");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var PersonResponse = await Res.Content.ReadAsStringAsync();

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    PersInfoOld = JsonConvert.DeserializeObject<List<Customer>>(PersonResponse);

                }

                //keep entries in List : with correct combination , delete all other entries 
                PersInfoOld.RemoveAll(x => x.FirstName != model.MVCFirstName || x.LastName != model.MVCLastName);

                //loop through result from new DB and delete the same entries from the result of old DB 
                foreach (var myIndex in PersInfo.Where(n=>n.BusinessEntityID != null )) 
                {
                    PersInfoOld.RemoveAll(x => x.BusinessEntityID == myIndex.BusinessEntityID);
                }

                foreach (var myIndex in PersInfoOld)
                {
                    myIndex.OldDatabase = true;
                    PersInfo.Add(myIndex);
                }

                
                HttpContext.Session.SetObjectAsJson("_Customer", PersInfo);


                if (PersInfo.Any()) //υπάρχει OldCustomer άρα View "CustomerOverview"
                {
                    return View("CustomerOverview", PersInfo);
                }
                else //δεν υπάρχει OldCustomer άρα View "CreateNewCustomer"
                {

                    ItemNewInfo.LastName = model.MVCLastName;
                    ItemNewInfo.FirstName = model.MVCFirstName;


                    //ItemNewInfo.Add(NewCustomer);
                    //NewInfo[1].FirstName = model.MVCFirstName;  //bring field attribute to New List in the next view
                    //NewInfo[1].LastName = model.MVCLastName;    //             >>           >>
                    return View("CreateNewCustomer", ItemNewInfo);
  
                }
               
            }

        }
        public IActionResult CreateNewCustomer(ModelClass model)
        {
            string babis = HttpContext.Session.GetString("FirstName");
            
            return View();
        }

        public IActionResult SelectCustomer(int id)
        {
            Customer ItemNewInfo = new Customer();
            CombinedModel Combo = new CombinedModel();
            var efcservice = new NewEFCService();
            var PersInfo = HttpContext.Session.GetObjectFromJson<List<Customer>>("_Customer");


            ItemNewInfo = PersInfo[PersInfo.FindIndex(a => a.BusinessEntityID == id)];

            //((ItemNewInfo.OldDatabase == true) ? return View("CreateNewCustomer", ItemNewInfo) : return View())


            Combo = efcservice.FetchProducts(ItemNewInfo);
            HttpContext.Session.SetObjectAsJson("_Combo", Combo);
            return (ItemNewInfo.OldDatabase == true) ? View("CreateNewCustomer", ItemNewInfo) : View("Order", Combo );



        }      




        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Landing()
        {
            return View();
        }


        public IActionResult SelectOldCustomer()
        {
            return View();
            
        }

        

        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
