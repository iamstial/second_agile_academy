﻿using CustomersAndOrdersCC.Data.Models;
using CustomersAndOrdersCC.NewData.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication
{
    public class NewEFCService
    {
           
        public List<Customer> SearchPerson( string FirstName  , string LastName)
        {
            using(var context = new MyNewDatabaseContext())
            {
                var query = from b in context.Person

                            join addcon in context.PersonAddressCon on b.PersonId equals addcon.PersonId

                            join address in context.Address on addcon.AddressId equals address.AddressId

                            where b.FirstName == FirstName && b.LastName == LastName

                            select new Customer()
                            {
                                BusinessEntityID = b.BusinessEntityId,
                                FirstName = b.FirstName,
                                LastName = b.LastName,
                                Gender = b.Gender,
                                //BirthDate
                                EmailAddress = b.EmailAddress,
                                HomePhoneNumber = b.PhoneNr,
                                AddressLine1 = address.Street + " " + address.HouseNr,
                                City = address.City,
                                PostalCode = address.PostalCode,
                                PersonID = b.PersonId

                            };

                return query.ToList();
            }
        }

        public CombinedModel FetchProducts(Customer cust)
        {
            using (var context = new MyNewDatabaseContext())
            {
                var query2 = from prod in context.Product

                             join unit in context.UnitType on prod.UnitTypeId equals unit.UnitTypeId

                             join prodtype in context.ProductType on prod.ProductTypeId equals prodtype.ProductTypeId

                             select new OrderingProduct()
                             {
                                 ProductId = prod.ProductId,
                                 ProductType = prodtype.Description,
                                 Description = prod.Description,
                                 UnitType = unit.Description,
                                 Price = prod.Price

                             };
                CombinedModel Combo = new CombinedModel();
                Combo.Oproduct = query2.ToList();
                Combo.PersInfo = cust;

                return Combo;
            }
        }
            
    

    }
}
