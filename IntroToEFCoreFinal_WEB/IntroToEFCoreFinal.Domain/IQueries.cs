﻿using IntroToEFCoreFinal.EFCDomain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IntroToEFCoreFinal.Domain
{
    public interface IQueries
    {
        IEnumerable<Person> Query1();

        IEnumerable<SalesOrderHeader> Query2();

    }
}
