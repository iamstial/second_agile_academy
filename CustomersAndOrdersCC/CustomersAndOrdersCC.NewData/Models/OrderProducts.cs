﻿using System;
using System.Collections.Generic;

namespace CustomersAndOrdersCC.NewData.Models
{
    public partial class OrderProducts
    {
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }

        public virtual Order Order { get; set; }
        public virtual Product Product { get; set; }
    }
}
