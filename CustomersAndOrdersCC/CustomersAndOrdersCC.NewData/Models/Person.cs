﻿using System;
using System.Collections.Generic;

namespace CustomersAndOrdersCC.NewData.Models
{
    public partial class Person
    {
        public Person()
        {
            Order = new HashSet<Order>();
            PersonAddressCon = new HashSet<PersonAddressCon>();
        }

        public Guid PersonId { get; set; }
        public int? BusinessEntityId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string PhoneNr { get; set; }
        public string EmailAddress { get; set; }
        public string Gender { get; set; }

        public virtual ICollection<Order> Order { get; set; }
        public virtual ICollection<PersonAddressCon> PersonAddressCon { get; set; }
    }
}
