﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersAndOrdersCC.Data.Models
{
    public class CombinedModel
    {
        public Customer PersInfo { get; set; }

        public List<OrderingProduct> Oproduct { get; set; }
        public Guid OrderID { get; set; }

    }
}
